#include "indexator_new.h"
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <string>
#include <vector>
#include <map>
using namespace std;

/*class TPos
{
public:
        int docid;
        long long pos;
        TPos (void);
        TPos(int, long long);
        TPos operator = (TPos &b);
};*/
class TDocWord{
public:
        int docid;
        vector<vector<long long>> positions;
        TDocWord(docidtype _docid);
        TDocWord operator=(TDocWord b);
};

void LoadIndex(map<string,vector<TPos>> &Index);

vector<TDocWord> DoSearch(vector<string> &s);
