#include "archieve.h"
#include "DoSearch.h"
#include "ranking.h"
#include <cstdio>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

const int NUMBER_OF_FACTORS = 5;
int weights[NUMBER_OF_FACTORS];

enum {
        TITLE = 0,
        ITALIC = 1,
        BOLD = 2,
        HEADING = 3,
        FREQUENCY = 4
};

void fillWeights() {
        weights[TITLE] = 10;
        weights[ITALIC] = 1;
        weights[BOLD] = 2;
        weights[HEADING] = 2;
        weights[FREQUENCY] = 12;
}

int addFromAttr (attr& wordAttr) {
        int res = 0;
        if (wordAttr.IsTitle())
                res += weights[TITLE];
        if (wordAttr.IsItalic())
                res += weights[ITALIC];
        if (wordAttr.IsBold())
                res += weights[BOLD];
        int levelH = 0;
        if (wordAttr.Ish3())
                levelH = 1;
        if (wordAttr.Ish2())
                levelH = 2;
        if (wordAttr.Ish1())
                levelH = 3;
        res += levelH * weights[HEADING];
        return res;
}

pair<int, pair<int, int> > cntPriority (const TDocWord& cur) {
        int res = 0;
        TDocument curDoc;
        curDoc.LoadDocument(cur.docid);
        if (curDoc.GetNumberOfSentences() == 0)
                return make_pair(0, make_pair(0, 0));
        size_t wordCount = cur.positions.size();
        vector <int> pointers(wordCount, 0);
        int numWord = 0;
        int haveWords = 0;
        vector<int> snippetSum(curDoc.GetNumberOfSentences());
        vector <int> beginPoses;
        int last = -1;
        for (TDocument::Iterator it = curDoc.begin(); !(it == curDoc.end()); ++it, ++numWord) {
                if (it.GetSentNum() > last) {
                        beginPoses.push_back(it.GetCurPos());
                        last = it.GetSentNum();
                }
                for (int i = 0; i < wordCount; ++i) {
                        while (pointers[i] < cur.positions[i].size() && cur.positions[i][pointers[i]] < numWord)
                                ++pointers[i];
                        if (pointers[i] < cur.positions[i].size() && cur.positions[i][pointers[i]] == numWord) {
                                attr wordAttr = it.GetAttr();
                                int add = addFromAttr(wordAttr);
                                res += add;
                                snippetSum[it.GetSentNum()] += add + weights[FREQUENCY];
                                ++haveWords;
                        }
                }
        }
        beginPoses.push_back(wordCount);
        res += (weights[FREQUENCY] * haveWords) / numWord;
        int maxSnippetSum = snippetSum[0];
        pair<int, int> best = make_pair(0, beginPoses[1]);
        for (int i = 0; i + 1 < snippetSum.size(); ++i) {
                if (snippetSum[i] + snippetSum[i + 1] > maxSnippetSum) {
                        best = make_pair(beginPoses[i], beginPoses[i + 2]);
                        maxSnippetSum = snippetSum[i] + snippetSum[i + 1];
                }
        }
        return make_pair(res, best);
}

vector<int> priority;
vector <pair<int, int> > begSnippet;

bool cmp (int q, int w) {
        return priority[q] > priority[w];
}

vector<int> ranking (vector<TDocWord>& list) {
        priority.resize(list.size());
        begSnippet.resize(list.size());
        vector <int> order(list.size());
        for (int i = 0; i < order.size(); ++i)
                order[i] = i;
        for (int i = 0; i < list.size(); ++i) {
                pair<int, pair<int, int> > now = cntPriority(list[i]);
                priority[i] = now.first;
                begSnippet[i] = now.second;
        }
        sort(order.begin(), order.end(), cmp);
        return order;
}

vector<pair<string, string> > doRankingAndSnippets(vector<string> query) { //âîçâðàùàåò (docid, url, snippet), âûçûâàåò ïîèñê äîêóìåíòîâ
        vector<TDocWord> list = DoSearch(query);
        TArchieve arch;
        arch.LoadArchieve();
        fillWeights();
        vector<int> order = ranking(list);
        vector<pair<string, string> > res;
        for (int i = 0; i < min((int)order.size(), 10); ++i) {
                int curIndex = order[i];
                TDocument curDoc;
                curDoc.LoadDocument(list[curIndex].docid);
                string path = arch.GetURL(list[curIndex].docid);
                res.push_back(make_pair(arch.GetURL(list[curIndex].docid), ""));
                for (int j = begSnippet[curIndex].first; j < begSnippet[curIndex].second; ++j) {
                        TDocument::Iterator it = curDoc.it(j);
                        res.back().second += it.GetName();
                        res.back().second += ' ';
                }
        }
        return res;
}

/*int main() {
        //freopen("output.txt", "w+", stdout);
        cout << "ranking starts\n";
        vector <string> query;
        query.push_back("timus");
        query.push_back("acm");
        vector<pair<pair<int, string>, string> > res = doRankingAndSnippets(query);
        for (int i = 0; i < res.size(); ++i) {
                cout << res[i].first.first << ' ' << res[i].first.second << endl;
                cout << res[i].second << endl;
        }
        cout << "ranking ends\n";
        return 0;
}*/
