/* (c) mephistopheles-search, 2013 */
/* Written by: Winogradov Mark     */

/* Inverted Index */

#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <vector>
#include <utility>
#include <algorithm>
#include <dirent.h>

using namespace std;

string FILE_KEYS = "./keys.o0",
       FILE_HEAP = "./heap.o0";

// container for word entry
class TPos
{
public:
                        // document id
                        string docid;
            // position in parsed file
            long long pos;
            TPos (void)
            {      docid = "", pos = 0;}
            TPos(string& _docid, long long _pos)
            {      docid = _docid, pos = _pos; }
};

class TIndex
{
        friend class TIndexIterator;
public:
      // constructs an index by given keys+heap files
      TIndex(string _keys_fn, string _heap_fn)
      {
                keys = new ifstream(_keys_fn);
                heap = new ifstream(_heap_fn);
                //if (!keys) return;
            while (!keys->eof())
            {
                  string word;
                  long long cnt;
                  //cout<<cnt<<'\n';
                  *keys >> word >> cnt >> cnt;
                  for (int i = 0; i < cnt; i++)
                  {
                    //cout<<i<<'\n';
                    //if (i>10) break;
                        TPos entry;
                        *heap >> entry.docid >> entry.pos;
                        index[word].push_back(entry);
                  }
            }
      }
     
private:
      //auto_ptr <ifstream> keys, heap;
      ifstream *keys, *heap;
      map <string, vector <TPos> > index;
};

class TIndexIterator
{
public:
        TIndexIterator(TIndex& Index, string Word)
        {
                cur = Index.index[Word].begin(),
                end = Index.index[Word].end();
        }

        // checks, whether the further iteration is valid
        bool valid(void)
        {
                return (cur != end);
        }

        // iterates by list of entries,
        // NEEDS TO BE CHECKED ON VALIDATION BEFORE!
        void next(void)
        {
                if (valid())
                        cur++;
        }

        // returns current entry,
        // NEEDS TO BE CHECKED ON VALIDATION BEFORE!
        TPos getPos(void)
        {
                if (valid())
                        return *cur;
        }
       
private:
        vector <TPos>::iterator cur, end;
};

/* build full index;
      pathi -- parsed database
          case_sensitive -- set, whether strings 'Ab' and 'AB' should be considered equal or not (true = NOT equal).
      patho -- output drectory; contains two files
            keys -- contains strings: <word> <position in heap> <count of entries in heap>
            heap -- contains strings: <document id> <position in parsed file>
      keys_fn, heap_fn -- corresponding file names
*/

bool handler (TPos a, TPos b) {
    return a.docid < b.docid || a.docid == b.docid && a.pos < b.pos;
}

void Build(string pathi, string patho, bool case_sensitive = true, string keys_fn = "keys.o0", string heap_fn = "heap.o0")
{
      map <string, vector <TPos> > wrd2pos;
      DIR* dir1 = opendir(pathi.c_str());
      FILE_KEYS = patho + keys_fn;
      FILE_HEAP = patho + heap_fn;
      ofstream keys (FILE_KEYS);
      ofstream heap (FILE_HEAP);
      long long pos = 0;
       //keys << "wdwefwef";
    for (struct dirent *d = readdir(dir1); d != NULL; d = readdir(dir1))
    {
        //keys << "1\n";
          if (d->d_name[0] == '.')
                  continue;

            string pte(d->d_name);
            pte = pathi + pte;
           
            ifstream f (pte);
            long long pos = 0;
            string docid;
            getline(f, docid);
            while (!f.eof())
            {
                  string temp = "";
                  getline(f, temp);
                  if (temp == "")
                        continue;
                                  //if (!case_sensitive)
                                        //transform(temp.begin(), temp.end(), temp.begin(), toupper);
                  wrd2pos[temp].push_back(TPos(docid, ++pos));
            }
      }
     
      for (auto it = wrd2pos.begin(); it != wrd2pos.end(); it++)
      {
        //keys << "2\n";
            keys << it->first << " " << pos << " ";
            int cnt = 0;
            sort(it->second.begin(), it->second.end(), handler);
            for (auto jt = it->second.begin(); jt != it->second.end(); jt++, cnt++)
            {
                  heap << jt->docid << " " << jt->pos + 1 << "\n";      
            }            
            keys << cnt << "\n";
            pos += cnt;
      }
     
      wrd2pos.clear();
      closedir(dir1);
      //keys.close(); heap.close();
      return void();      
}


int main(){ // "BaseNew/", "./o/"
    Build("../Normal/BaseNorm/", "./o/");
    return 0;
}