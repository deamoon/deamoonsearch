#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import cgi
import datetime
import string
import binascii

sys.stderr = sys.stdout
import cgitb; cgitb.enable()

import json


def printStartPageRedir(form):
    print "<body><meta http-equiv='refresh' content='0; url=?'></body>"

def printDoSearch(form):
    #print "Вы ввели запрос [%s]" % form.getfirst("query", "")
    import xmlrpclib
    s = xmlrpclib.ServerProxy('http://search.vnik.me:8000')
    input_string = form.getfirst("query", "")
    answer = s.fake_search(input_string)
    
    fs = cgi.FieldStorage()
    sys.stdout.write("Content-Type: application/json")
    sys.stdout.write("\n")
    sys.stdout.write("\n")
    result = {}
    #result['success'] = True
    result['message'] = answer.encode('utf-8')
    #result['keys'] = ",".join(fs.keys())

    sys.stdout.write(json.dumps(result,indent=1))
    sys.stdout.write("\n")
    sys.stdout.close()
    
    #print "<div class=font>"
    #print ("server return: " + answer.encode('utf-8'))
    #print "</div>"    


def printStartPage(form):
    print "<div align=center>"
    print "<form id=search_form action="" name=search accept-charset=utf-8><fieldset>"
    #print "<input style='display:none' name=action value=dosearch>"
    print "<input type=text class=search-query placeholder=acm.timus.ru autocomplete=off id=search data-provide=typeahead data-items=4 data-source='[Alaska]'>"   
    print "<input type=submit id=clickme class=btn value='Искать'>"
    
    print "</fieldset></form>"
    #print "<button id=clickme class=btn>Искать</button>"
    print "</div>"
    print "<div id=content></div>"
    #print "<button id=clickme class=btn>Искать</button>"
    print "<div id=users>"
    print "<dl>"
    print "</dl>"
    print "</div>"
    print "<script src=/js/my.js></script>"

def printBody(act, form):
    print "<body>"
    if act == "dosearch":
        printDoSearch(form)
    else:
        printStartPage(form)
    print "</body>"

def printHTML(act, form):
    print "Content-type: text/html; charset=utf-8\n"
    print "<html><head><title>Поиск</title>"
    print "<meta charset=utf-8>"
    print '<link href="/css/main.css" rel="stylesheet" type="text/css" />'
    print "<link href=/css/bootstrap.css rel=stylesheet media=screen>"
    print "<script src=/js/jquery.js></script>"
    print "<script src=/js/bootstrap.min.js></script>"
    print "<script src=/js/ajax.js></script>"
    print "</head>"
    printBody(act, form)
    print "</html>"

def main():
    form = cgi.FieldStorage()
    act = form.getfirst("action", "")
    printHTML(act, form)

main()

