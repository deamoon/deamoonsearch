#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool IsGoodSymbol(char ch) {
	if (('0' <= ch && ch <= '9') || 
		('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z') ||
		(-48 <= ch && ch <= 'я') || ('А' <= ch && ch <= 'Я')) 
	{
		return true;	
	} else {
		return false;
	}
}

void Word(const string &s, int &pos, string &word) {
	word.clear();
	while (pos < (int)s.size() && IsGoodSymbol(s[pos])) {
		word.push_back(s[pos]);
		++pos;
	}
}

void Run(const string &s, int &pos) {
	while (pos < (int)s.size() && !IsGoodSymbol(s[pos])) {
		++pos;		
	}
}

void NoName(const string &s, vector<string> &vs) {
	vs.clear();
	
	if (s.size() == 0) {
		return ;
	}
	
	int pos = 0;
	
	while (pos < (int)s.size()) {
		string word;
		Word(s, pos, word);
		if ((int)word.size() != 0) {
			vs.push_back(word);
		}
		Run(s, pos);
	}	
}

void Print(const vector<string> &vs) {
	for (int i = 0; i < (int)vs.size(); ++i) {
		cout << vs[i] << endl;
	}
}

int main() {
	/*freopen("input.txt", "r", stdin);
	
	string s;
	getline(cin, s);
	
	vector<string> vs;
	NoName(s, vs);
	
	Print(vs);
	
	*/
	
	char ch
	cin >> ch;
	
	cout << ch << endl;
	
	return 0;
}
