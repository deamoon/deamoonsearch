#include "DoSearch.h"
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <string>
#include <vector>
#include <map>
using namespace std;

/*TPos::TPos (void) {
        docid = 0, pos = 0;
}
TPos::TPos(int _docid, long long _pos):docid(_docid), pos(_pos) {}
TPos TPos::operator =(TPos &b) {
        docid=b.docid, pos=b.pos; return *this;
}*/

TDocWord::TDocWord(docidtype _docid): docid(_docid){}
TDocWord TDocWord::operator=(TDocWord b)
{
        docid=b.docid;
        positions=b.positions;
        return *this;
}

/*void LoadIndex(map<string,vector<TPos>> &Index)
{
        int n;
        cin>>n;
        for(int i=0;i<n;i++)
        {
                int m;
                string Word;
                cin>>Word;
                cin>>m;
                vector<TPos> a;
                a.resize(m);
                long long val;
                for(int j=0;j<m;j++)
                {
                        cin>>a[j].docid;
                        cin>>a[j].pos;
                }
                Index[Word]=a;
        }
}*/

void LoadIndex(const vector<string> &s, vector<vector<TPos> > &a)
{
        int n=s.size();
        for(int i=0;i<n;i++)
        {
                TIndexIterator Iter(q,s[i]);
                vector<TPos> cur;
                while (Iter.valid())
                {
                        TPos res=Iter.getPos();
                        cur.push_back(res);
                        //cout<<res.docid<<" "<<res.pos<<"\n";
                        Iter.next();
                }
                a.push_back(cur);
        }
}


vector<TDocWord> DoSearch(vector<string> &s)
{
        vector<vector<TPos> > a;
        LoadIndex(s, a);

        vector<TDocWord> ans;
        vector<int> iter(a.size());

        for(;iter[0]!=a[0].size();iter[0]++){
                bool flag=true;
                docidtype CurDocID=a[0][iter[0]].docid;
                for(int i=1;i<a.size();i++)
                {
                        while(a[i][iter[i]].docid<CurDocID)
                                iter[i]++;
                        if((a[i][iter[i]].docid)!=CurDocID) {
                                flag=false;
                                break;
                        }
                }
                if(flag)
                {
                        TDocWord CurDoc(CurDocID);
                        CurDoc.positions.resize(a.size());
                        for(int i=0;i<a.size();i++)
                        {
                                while(iter[i]<a[i].size() && a[i][iter[i]].docid==CurDocID){
                                        CurDoc.positions[i].push_back(a[i][iter[i]].pos);
                                        iter[i]++;
                                }
                        }
                        ans.push_back(CurDoc);
                        iter[0]--;
                }
        }
        return ans;
}

/*int main ()
{
        freopen("input.txt","r",stdin);
        vector<string> s;
        s.push_back("timus");
        s.push_back("acm");
        vector<TDocWord> r = DoSearch(s);
        return 0;
}*/

/*
 TIndex q("./o/keys.o0", "./o/heap.o0");
      cout << "index constructed.\nmaking iterators.\n";
         
          TIndexIterator timus(q, "Timus"),
                                         acm  (q, "ACM");
     
          cout << "proceeding: 'Timus'\n";
          while (timus.valid())
      {
            TPos res = timus.getPos();
            cout << res.docid << " " << res.pos << "\n";
                        timus.next();
      }

          cout << "\n-----\nproceeding: 'ACM'\n";
          while (acm.valid())
      {
            TPos res = acm.getPos();
            cout << res.docid << " " << res.pos << "\n";
                        acm.next();
      }
     
      cout << "\n-----\nfinished.\n";
*/
