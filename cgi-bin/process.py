#!/usr/bin/python
# -*- coding: utf-8 -*-
import commands, os, string, json, sys

def FindProcess(program):
    output = commands.getoutput("ps -A | grep " + program)
    proginfo = string.split(output)
    if (output) :
        return proginfo[0] 
    else:
        return 0

sys.stdout.write("Content-Type: application/json")
sys.stdout.write("\n")
sys.stdout.write("\n")
result = {}
result['index_server.py'] = FindProcess("index_server.py")
result['ajax.py'] = FindProcess("ajax.py")
sys.stdout.write(json.dumps(result,indent=1))
sys.stdout.write("\n")
sys.stdout.close()

