#include <stdio.h> 
#include <libxml/parser.h> 
#include <libxml/HTMLparser.h> 
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

///////////////////////////////////////////////////////////////////

int next_word(xmlNode * a_node) {
  xmlNode *cur_node = NULL;
  std::string Name;
  for (cur_node = a_node; cur_node; cur_node = cur_node->next) {    
      
      if ((cur_node -> content) && (cur_node->name)) {
          Name = (char *) cur_node -> content
          Cutter(Name);
      }
      
      next_word(cur_node->children);
  }
  
  return 0;
}

////////////////////////////////////////////////////////////////////

int main(int ac, char* av[]) {
  xmlNode *root_element = NULL;
  std::string file("1.html");
  htmlDocPtr docPtr = htmlParseFile(file.c_str(), "utf-8");
  
  root_element = xmlDocGetRootElement(docPtr);
  next_word(root_element);
 
  xmlFreeDoc(docPtr); xmlCleanupParser();
  return 0;
} 
