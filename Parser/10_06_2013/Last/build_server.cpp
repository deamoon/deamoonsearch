/* (c) mephistopheles-search, 2013 */
/* Written by: Winogradov Mark     */

/* Inverted Index */

#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <vector>
#include <utility>
#include <algorithm>
#include <dirent.h>

using namespace std;

string FILE_KEYS = "./keys.o0",
           FILE_HEAP = "./heap.o0";

// container for word entry
class TPos
{
        public:
                // document id
                string docid;
                // position in parsed file
                long long pos;
                TPos (void)
                {       docid = "", pos = 0;}
                TPos(string& _docid, long long _pos)
                {       docid = _docid, pos = _pos; }
};

/* build full index;
        pathi -- parsed database
        patho -- output drectory; contains two files
                keys -- contains strings: <word> <position in heap> <count of entries in heap>
                heap -- contains strings: <document id> <position in parsed file>
        keys_fn, heap_fn -- corresponding file names.
*/
bool handler (TPos a, TPos b) {
    return a.docid < b.docid || a.docid == b.docid && a.pos < b.pos;
}


void Build(string pathi, string patho, string keys_fn = "keys.o0", string heap_fn = "heap.o0")
{
        map <string, vector <TPos> > wrd2pos;
        DIR* dir1 = opendir(pathi.c_str());
        FILE_KEYS = patho + keys_fn;
        FILE_HEAP = patho + heap_fn;
        ofstream keys (FILE_KEYS);
        ofstream heap (FILE_HEAP);
        long long pos = 0;
         
    for (struct dirent *d = readdir(dir1); d != NULL; d = readdir(dir1))
    {
        if (d->d_name[0] == '.')
                        continue;

                string pte(d->d_name);
                pte = pathi + pte;
               
                ifstream f (pte);
                long long pos = 0;
                string docid;
                getline(f, docid);
                while (!f.eof())
                {
                        string temp = "";
                        getline(f, temp);
                        if (temp == "")
                                continue;
                        wrd2pos[temp].push_back(TPos(docid, ++pos));
                }
        }
       
        for (auto it = wrd2pos.begin(); it != wrd2pos.end(); it++)
        {
                keys << it->first << " " << pos << " ";
                int cnt = 0;
                sort(it->second.begin(), it->second.end(), handler);
                for (auto jt = it->second.begin(); jt != it->second.end(); jt++, cnt++)
                {
                        heap << jt->docid << " " << jt->pos + 1 << "\n";        
                }              
                keys << cnt << "\n";
                pos += cnt;
        }
       
        wrd2pos.clear();
       
        return void();  
}

void index_build(){
    Build("BaseNew/", "./o/");
}

int main() {
    
    index_build(); // Один раз используется
   
    return 0;
}






