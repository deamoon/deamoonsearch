/* (c) mephistopheles-search, 2013 */
/* Written by: Winogradov Mark     */

/* Inverted Index */

#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <vector>
#include <utility>
#include <algorithm>
#include <dirent.h>
#include <boost/python.hpp>

using namespace std;

string FILE_KEYS = "./keys.o0",
           FILE_HEAP = "./heap.o0";

// container for word entry
class TPos
{
        public:
                // document id
                string docid;
                // position in parsed file
                long long pos;
                TPos (void)
                {       docid = "", pos = 0;}
                TPos(string& _docid, long long _pos)
                {       docid = _docid, pos = _pos; }
};

class TIndex
{
public:
        // constructs an index by given keys+heap files
        TIndex(string _keys_fn, string _heap_fn)
        {
                keys = new ifstream(_keys_fn);
                heap = new ifstream(_heap_fn);
                while (!keys->eof())
                {
                        string word;
                        long long cnt;
                        *keys >> word >> cnt >> cnt;
                        for (int i = 0; i < cnt; i++)
                        {
                                TPos entry;
                                *heap >> entry.docid >> entry.pos;
                                index[word].push_back(entry);
                        }
                }
        }

        // checks, whether a further iteration by the given word is valid
        bool valid(string word)
        {
                if (index.find(word) == index.end() || cnt[word] >= index[word].size())
                        return false;
                return true;
        }
        // returns an entry of a given word
        // if next set true, iterates to the next entry, NEEDS TO BE CHECKED BY valid()
        TPos getPos(string word, bool next = false)
        {
                if (cnt.find(word) == cnt.end())
                        cnt[word] = 0;
               
                if (valid(word))
                        return index[word][((next) ? cnt[word]++ : cnt[word])];
                if (next)
                        cnt[word]++;
                return TPos();
        }
        // iterates to the next entry, NEEDS TO BE CHECKED BY valid()
        void next(string word)
        {
                cnt[word]++;
        }
       
private:
        ifstream *keys, *heap;
        map <string, vector <TPos> > index;
        map <string, int> cnt;
};

/* build full index;
        pathi -- parsed database
        patho -- output drectory; contains two files
                keys -- contains strings: <word> <position in heap> <count of entries in heap>
                heap -- contains strings: <document id> <position in parsed file>
        keys_fn, heap_fn -- corresponding file names.
*/
bool handler (TPos a, TPos b) {
    return a.docid < b.docid || a.docid == b.docid && a.pos < b.pos;
}


void Build(string pathi, string patho, string keys_fn = "keys.o0", string heap_fn = "heap.o0")
{
        map <string, vector <TPos> > wrd2pos;
        DIR* dir1 = opendir(pathi.c_str());
        FILE_KEYS = patho + keys_fn;
        FILE_HEAP = patho + heap_fn;
        ofstream keys (FILE_KEYS);
        ofstream heap (FILE_HEAP);
        long long pos = 0;
         
    for (struct dirent *d = readdir(dir1); d != NULL; d = readdir(dir1))
    {
        if (d->d_name[0] == '.')
                        continue;

                string pte(d->d_name);
                pte = pathi + pte;
               
                ifstream f (pte);
                long long pos = 0;
                string docid;
                getline(f, docid);
                while (!f.eof())
                {
                        string temp = "";
                        getline(f, temp);
                        if (temp == "")
                                continue;
                        wrd2pos[temp].push_back(TPos(docid, ++pos));
                }
        }
       
        for (auto it = wrd2pos.begin(); it != wrd2pos.end(); it++)
        {
                keys << it->first << " " << pos << " ";
                int cnt = 0;
                sort(it->second.begin(), it->second.end(), handler);
                for (auto jt = it->second.begin(); jt != it->second.end(); jt++, cnt++)
                {
                        heap << jt->docid << " " << jt->pos + 1 << "\n";        
                }              
                keys << cnt << "\n";
                pos += cnt;
        }
       
        wrd2pos.clear();
       
        return void();  
}

void index_build(){
    Build("../parser/BaseNew/", "./o/");
}

TIndex& index_T(){
    TIndex *q = new TIndex("./o/keys.o0", "./o/heap.o0");
    return *q;
}

string indexator(string s, TIndex &q) {
    //TIndex q("./o/keys.o0", "./o/heap.o0");
    while (q.valid(s)) {
        TPos res = q.getPos(s, true);
        //cout << res.docid << " " << res.pos << "\n";
        return res.docid;
    }
}

class TPos2
{
        public:
                // document id
                string docid;
                // position in parsed file
                long long pos;
                TPos2 (void)
                {       docid = "", pos = 0;}
                TPos2(string& _docid, long long _pos)
                {       docid = _docid, pos = _pos; }
};

using namespace boost::python;

BOOST_PYTHON_MODULE(index) {
    //boost::python::def("indexator", indexator);
    //boost::python::def("index_T", index_T);
    boost::python::def("index_build", index_build);
    /*class_<TIndex>("TIndex")
        .def("greet", &World::greet)
        .def("set", &World::set)
    ;*/
    class_<TPos2>("TPos2")
        .def_readwrite("pos", & TPos2::pos)
        .def_readwrite("docid", & TPos2::docid)
        .def(init<copy_non_const_reference, long long>())
        //.def( "Name", &Some::Name, return_value_policy<copy_const_reference>() )
    ;       
}




/*class TPos
{
        public:
                // document id
                string docid;
                // position in parsed file
                long long pos;
                TPos (void)
                {       docid = "", pos = 0;}
                TPos(string& _docid, long long _pos)
                {       docid = _docid, pos = _pos; }
};*/










