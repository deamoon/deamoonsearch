#include "indexator_new.h"
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <deque>
#include <vector>
#include <utility>
#include <algorithm>
#include <dirent.h>
#include <boost/python.hpp>

using namespace std;

unordered_map<string, int> link_int;
vector<string> int_link;

void init() {
  int_link.push_back("timus");
  ifstream link_info("link.info");
  int k = 0; string s;
  while (getline(link_info, s)) {
      link_int[s] = ++k;
      int_link.push_back(s);
  }
  link_info.close();
}

bool handler (TPos a, TPos b) {
    return a.docid < b.docid || a.docid == b.docid && a.pos < b.pos;
}

string FILE_KEYS = "./keys.o0",
       FILE_HEAP = "./heap.o0";


TPos::TPos (void)
{      docid = 0, pos = 0;}
TPos::TPos(docidtype& _docid, long long _pos)
{      docid = _docid, pos = _pos; }

TIndex::TIndex(string _keys_fn, string _heap_fn)
{
    init();
    keys = new ifstream(_keys_fn);
    heap = new ifstream(_heap_fn);
    while (!keys->eof())
    {
            string word;
            long long cnt;
            *keys >> word >> cnt >> cnt;
            for (int i = 0; i < cnt; i++)
            {
                TPos entry;
                *heap >> entry.docid >> entry.pos;
                index[word].push_back(entry);
            }
    }
}

TIndexIterator::TIndexIterator(TIndex& Index, string Word)
{
        cur = Index.index[Word].begin(),
        end = Index.index[Word].end();
}
bool TIndexIterator::valid(void)
{
        return (cur != end);
}
void TIndexIterator::next(void)
{
if (valid())
cur++;
}
TPos TIndexIterator::getPos(void)
{
    if (valid())
        return *cur;
}

void Build(string pathi, string patho, bool case_sensitive = true, string keys_fn = "keys.o0", string heap_fn = "heap.o0")
{
      unordered_map <string, vector <TPos> > wrd2pos;
      DIR* dir1 = opendir(pathi.c_str());
      FILE_KEYS = patho + keys_fn;
      FILE_HEAP = patho + heap_fn;
      ofstream keys (FILE_KEYS);
      ofstream heap (FILE_HEAP);
      long long pos = 0;
       
    int docid = 0;
    for (struct dirent *d = readdir(dir1); d != NULL; d = readdir(dir1))
    {
          if (d->d_name[0] == '.')
                  continue;

            string pte(d->d_name);
            pte = pathi + pte;
           
            ifstream f (pte);
            long long pos = 0;
            docid++;
   //         getline(f, docid);
            while (!f.eof())
            {
                  string temp = "";
                  getline(f, temp);
                  if (temp == "")
                        continue;
                  //if (!case_sensitive)
                        //transform(temp.begin(), temp.end(), temp.begin(), toupper);
                  wrd2pos[temp].push_back(TPos(docid, ++pos));
            }
      }
     
      for (auto it = wrd2pos.begin(); it != wrd2pos.end(); it++)
      {
            keys << it->first << " " << pos << " ";
            int cnt = 0;
            sort(it->second.begin(), it->second.end(), handler);
            for (auto jt = it->second.begin(); jt != it->second.end(); jt++, cnt++)
            {
                  heap << jt->docid << " " << jt->pos + 1 << "\n";      
            }            
            keys << cnt << "\n";
            pos += cnt;
      }
     
      wrd2pos.clear();
      closedir(dir1);
   
      return void();      
}

void index_build(){
    Build("BaseNew/", "./o5/");
}

int min(int a, int b) {
    if (a<b) return a; else return b;
}

struct link {
    int id;
    int kol;
    link():id(0), kol(0) {}
};

bool handler_sort(struct link a, struct link b) {
    return (a.id < b.id);
}

bool handler_pair(pair <int, int> a, pair <int, int> b) {
    return (a.second > b.second);
}

string IntToStr(int a) {
        string s;
        while (a>0) {
                s+=a%10+'0';
                a/=10;
        }
        reverse(s.begin(),s.end());
        return s;
}

docidtype index_server2(string mes) {  
    TIndexIterator timus(q, mes);
    while (timus.valid()) {
        TPos res = timus.getPos();
        //cout << res.docid << " " << res.pos << "\n";
        //timus.next();
        return res.docid;
    }
    return -1; // "NULL"
}

void index_test(string & mes, vector < vector < struct link > > & A) {
    A.push_back(vector<struct link>());
    TIndexIterator timus(q, mes);
    int t = 1, id, size;
    struct link url;

    while (timus.valid()) {
        TPos res = timus.getPos();
        cout << res.docid<<" ";
        timus.next();
    }

}

void index_map(string & mes, vector < unordered_map < int, int > > & A) {
    A.push_back(unordered_map<int, int>());
    TIndexIterator timus(q, mes);
    int t = 1, id;
    struct link url;
    while (timus.valid()) {
        TPos res = timus.getPos();
        if (!timus.valid()) break;
        //url.id = id = link_int[res.docid]; 
        url.id = id = res.docid; 
        int kol = 0;
        
        //while (link_int[res.docid] == id) {
        while (res.docid == id) {
            ++kol;
            timus.next();    
            if (!timus.valid()) break;
            res = timus.getPos();
        }
        url.kol = kol;
        
        A[A.size() - 1][url.id] = url.kol;
        t = 0;
    }
    if (t) {
        A.pop_back();
    }
}

string index_server(string mes) {
  //index_build();
  istringstream str_in(mes);
  string s;
  int id, kol, num;
  vector < unordered_map < int, int > > A;
  vector < pair < int, int > > res_sort;
  unordered_map<int, int> res;
  init();
  while (str_in>>s) {
      index_map(s, A);
  }

  //cout<<A.size()<<endl; 
  if (A.size() == 0) return "NULL";

  for (int k = 0; k < A.size(); ++k) {
    for (unordered_map<int, int>::iterator it_k = A[k].begin(); it_k!=A[k].end(); ++it_k) {
      id = (*it_k).first; //val = (*it_k).second
      kol = 0; num = 0;
      if (res.count(id)) continue;
      for (int i = 0; i < A.size(); ++i) {
        unordered_map<int, int>::iterator it = A[i].find(id);
        if (it != A[i].end()) {
          ++num;
          kol += (*it).second; // Formula
        }  
      }
      kol += 1000 * num; // Bonus
      res[id] = kol;
    }
  }
  
  /*for (int k = 0; k < A.size(); ++k) {
    for (unordered_map<int, int>::iterator it_k = A[k].begin(); it_k!=A[k].end(); ++it_k)  {
      cout<<(*it_k).first<<"-"<<(*it_k).second<<"  ";
    } 
    cout<<endl; 
  }
  cout<<endl;*/
  for (unordered_map<int, int>::iterator it_k = res.begin(); it_k!=res.end(); ++it_k) {
      res_sort.push_back(*it_k);
  }
  sort(res_sort.begin(), res_sort.end(), handler_pair);
  string res_str = "";
  for (int i = 0; i < min(res_sort.size(),10); ++i) {
      res_str += int_link[res_sort[i].first] + '|' + IntToStr(res_sort[i].first) + '|';
  }
  return res_str;
}

using namespace boost::python;
BOOST_PYTHON_MODULE(index_server) {
    boost::python::def("index_build", index_build);
    boost::python::def("index_server", index_server);
}