#!/usr/bin/python3
import xmlrpc.client

s = xmlrpc.client.ServerProxy('http://search.vnik.me:8000')

input_string = input()

print(s.pow(2,3))  # Returns 2**3 = 8
print(s.add(2,3))  # Returns 5
print(s.mul(5,2))  # Returns 5*2 = 10

answer = s.fake_search(input_string)
print ("server return: " + answer)

'''
# Print list of available methods
print(s.system.listMethods())
'''
