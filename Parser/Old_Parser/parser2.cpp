#include "Cutter.h"
#include <stdio.h>
#include <libxml/parser.h> 
#include <libxml/HTMLparser.h> 
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

///////////////////////////////////////////////////////////////////

int next_word(xmlNode * a_node) {
  xmlNode *cur_node = NULL;
  std::string Name;
  for (cur_node = a_node; cur_node; cur_node = cur_node->next) {    
      
      if ((cur_node -> content) && (cur_node->name)) {
          Name = (char *) cur_node -> content;
          //std::cout<<Name<<'\n'; 
          Cutter(Name); 
          //std::cout<<'\n';
      }
      
      next_word(cur_node->children);
  }
  
  return 0;
}

////////////////////////////////////////////////////////////////////

int main(int ac, char* av[]) {
  std::ifstream link_info ("link.info"); std::ostringstream oss;
  xmlNode *root_element = NULL;

  int max=0;

  std::string str_link, file, file_out; int k = 0;
  while (getline(link_info, str_link)) {
      oss<<++k; file = "../../Fetcher/Base/" + oss.str() + ".html"; file_out = "../BaseNew/" + oss.str() + ".info"; oss.str(""); // получаем название файла по числу      
      freopen(file_out.c_str(),"w",stdout);
      std::cout<<str_link<<":\n";
      htmlDocPtr docPtr = htmlParseFile(file.c_str(), "utf-8");    
      root_element = xmlDocGetRootElement(docPtr);
      next_word(root_element);
      xmlFreeDoc(docPtr); 
      //++max; if (max>=5) break;
  }  
  
  xmlCleanupParser(); link_info.close();
  return 0;
} 













