#include "Cutter.h"
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

bool IsGoodSymbol(char ch) {
	if (    ('0' <= ch && ch <= '9') || 
		('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z') ||
		//('а' <= ch && ch <= 'я') || ('А' <= ch && ch <= 'Я') ||
		(4294967248 <= (unsigned int)ch && (unsigned int)ch <= 4294967249) ||
		(4294967168 <= (unsigned int)ch && (unsigned int)ch <= 4294967231)
	   )	
	{
		return true;	
	} else {
		return false;
	}
}

void Word(const std::string &s, int &pos, std::string &word) {
	word.clear();
	while (pos < (int)s.size() && IsGoodSymbol(s[pos])) {
		word.push_back(s[pos]);
		++pos;
	}
}

void Run(const std::string &s, int &pos) {
	while (pos < (int)s.size() && !IsGoodSymbol(s[pos])) {
		++pos;		
	}
}

void NoName(const std::string &s, std::vector<std::string> &vs) {
	vs.clear();
	
	if (s.size() == 0) {
		return ;
	}
	
	int pos = 0;
	
	while (pos < (int)s.size()) {
		std::string word;
		Word(s, pos, word);
		if ((int)word.size() != 0) {
			vs.push_back(word);
		}
		Run(s, pos);
	}	
}

int Qt(int a) {
	return a + 4294967000;
}

char lowcase(std::string &s, int i) {
	char c = s[i];
	int n = (unsigned int)c;
	if ( ((Qt(184) <= n) && (n <= Qt(215))) || (n == Qt(169)) ) {	
        if ( (Qt(184) <= n) && (n <= Qt(199)) ) s[i] = (char)(n+32);	
        if ( n==Qt(169) ) { s[i] = (char)(Qt(185)); s[i-1] = (char)(Qt(249)); }
        if ( (Qt(200) <= n) && (n <= Qt(215)) ) { s[i] = (char)(n-32); s[i-1] = (char)(Qt(249)); }		
	}
}

void LowCase(std::string &s){
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	for (int i=0;i<s.length();++i) {
        if ( ((unsigned int)s[i]==4294967248) && (i!=s.length()-1) ) {
            lowcase(s, i+1);	
            continue;
        }
	}
}

void Print(std::vector<std::string> &vs) {
	for (int i = 0; i < (int)vs.size(); ++i) {
		LowCase(vs[i]);
		std::cout << vs[i] << '\n';
		//NextWord(vs[i]);
	}
}

int Cutter(std::string &s) {
	std::vector<std::string> vs;
	NoName(s, vs);
	Print(vs);
	return 0;
}