#!/usr/bin/python

import sys
import re
import json
import commands
import cgi
import os
import datetime
import string
import binascii
sys.stderr = sys.stdout
import cgitb; cgitb.enable()
import xmlrpclib

form = cgi.FieldStorage()
act = form.getfirst("action", "")

if act == "dosearch":
	query = form.getfirst("query", "")
	#sys.stdout.write(query)
	s = xmlrpclib.ServerProxy('http://search.vnik.me:8000')
	
	p = re.compile('[{}|?]')
	p2 = re.compile('[|]')
	res = ""
	res_full = ""
	q = 1
	mystem = commands.getoutput("echo " + query + " | ./mystem -e utf-8")
	for i in p.split(mystem):
		if i:
			res_full = res_full + i + " "
			if q:
				q = 0
				res = i
    
	#res = query+" "
	res_full = res_full[:-1]
	#answer = s.fake_search(res)
	answer = s.fake_search(res_full)
	answer = answer.encode('utf-8')

	sys.stdout.write("Content-Type: application/json")
	sys.stdout.write("\n")
	sys.stdout.write("\n")
	t = -1
	result = {}
	for i in p2.split(answer):
		if i:
			t = t + 1
			if (t % 2 == 0):
				result['mes' + str(t/2)] = i
			else:
				result['mes' + str(t/2) + '_num'] = i	
	#result['message'] = answer.encode('utf-8')
	result['query'] = res_full
	#result['message'] = query
	sys.stdout.write(json.dumps(result,indent=1,sort_keys=True))
	sys.stdout.write("\n")
	sys.stdout.close()
else:
	sys.stdout.write("Content-Type: application/json")
	sys.stdout.write("\n")
	sys.stdout.write("\n")
	result = {}
	result['message'] = "NULL"
	sys.stdout.write(json.dumps(result,indent=1))
	sys.stdout.write("\n")
	sys.stdout.close()
