#include "archieve.h"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
using namespace std;

void TArchieve::LoadArchieve() {
               
        /*freopen("info","r",stdin);
        cin>>file_amount;*/
        FILE* f;
        f=fopen("info","r");
        fscanf(f,"%d",&file_amount);
        char* u=new char[file_amount+2]();
               
        urls.resize(file_amount+1);
        for (int i=0; i<file_amount; i++) {
                int ind;
                string url="";
                fscanf(f,"%d",&ind);
                fscanf(f,"%s",u);
                int l=0;
                while (u[l]!=0) {
                        url+=u[l];
                        l++;
                }
                urls[ind]=url;
        }
        fclose(f);
}
string TArchieve::GetURL(int ID) {
        return urls[ID];
}
void TArchieve::clear() {
        file_amount=0;
        urls.clear();
}

string IntToStr(int a) {
        string s;
        while (a>0) {
                s+=a%10+'0';
                a/=10;
        }
        reverse(s.begin(),s.end());
        return s;
}

void attr::fillFlags(int mask) {
        //cout<<mask<<endl;
        int a=mask;
        b=a%2;
        a/=2;
        i=a%2;
        a/=2;
        title=a%2;
        a/=2;
        h3=a%2;
        a/=2;
        h2=a%2;
        a/=2;
        h1=a%2;
        /*if ((mask&32)!=0) {h1=1;}
        if ((mask&16)!=0) {h2=1;}
        if ((mask&8)!=0) {h3=1;}
        if ((mask&4)!=0) {title=1;}
        if ((mask&2)!=0) {i=1;}
        if ((mask&1)!=0) {b=1;}*/

}
bool attr::Ish1() {return h1;}
bool attr::Ish2() {return h2;}
bool attr::Ish3() {return h3;}
bool attr::IsTitle() {return title;}
bool attr::IsItalic() {return i;}
bool attr::IsBold() {return b;}

void TDocument::LoadDocument(int ID_) {
        word_amount=0;
        string file;
               
        file=IntToStr(ID_);
               
        char* file_name=new char[file.length()+20]();
        file_name = "GoodBase/";
        for (int i=0; i<file.length(); i++) {
                file_name[i + 9]=file[i];
        }
        file_name[file.length() + 9] = 0;
        FILE* f=fopen(file_name,"r");
        ID=ID_;
        fscanf(f,"%d",&sent_amount);
        //cin>>sent_amount; //amount of sentences in file
        for (int sent_ind=0; sent_ind<sent_amount; sent_ind++) {
                int i;
                fscanf(f,"%d",&i);
                word_amount+=i;
                for (int word_ind=0; word_ind<i; word_ind++) {
                        attr cur_attr;
                        sent_num.push_back(sent_ind);
                        word_num.push_back(word_ind);
                        string word="";
                        int mask;
                        char* u=new char[200]();
                        fscanf(f,"%s",u);
                        fscanf(f,"%d",&mask);
                        //fclose(f);
                               
                        int l=0;
                        while (u[l]!=0) {
                                word+=u[l];
                                l++;
                        }
                        free(u);
                        //cin>>word>>mask;
                        words.push_back(word);
                        cur_attr.fillFlags(mask);
                        attrs.push_back(cur_attr);
                }
        }
}

TDocument::Iterator TDocument::begin() {
        Iterator it(this,0);    
        return it;
};
TDocument::Iterator TDocument::end() {
        Iterator it(this,word_amount-1);
        return it;
};
TDocument::Iterator TDocument::it(int pos) {
        if (pos>0 && pos<=word_amount) {
                Iterator it(this,pos);
                return it;
        } else {
                exit(1);
        }
}
void TDocument::clear() {
        attrs.clear();
        sent_num.clear();
        word_num.clear();
        words.clear();
        ID=0;
        sent_amount=0;
        word_amount=0;
}

void TDocument::Iterator::operator ++ () {
                pos++;
}
bool TDocument::Iterator::operator == (Iterator a) {
        if (pos==a.pos && Doc==a.Doc) {return true;} else {return false;}
}
TDocument::Iterator::Iterator(TDocument* Doc_, int pos_) {
        pos=pos_;
        Doc=Doc_;
}
string TDocument::Iterator::GetName() {
        return Doc->words[pos];
}
attr TDocument::Iterator::GetAttr() {
        return Doc->attrs[pos];
}
int TDocument::Iterator::GetSentNum() {
        return Doc->sent_num[pos];
}
int TDocument::Iterator::GetWordNum() {
        return Doc->word_num[pos];
}
int TDocument::Iterator::GetCurPos() {
        return pos;
}

/*int main() {
        freopen("output","w",stdout);
        TArchieve arch;
        arch.LoadArchieve();
        cout<<arch.GetURL(1)<<endl;
        TDocument Doc;
        Doc.LoadDocument(1);
        for (TDocument::Iterator i=Doc.begin(); !(i==Doc.end()); i++) {
                cout<<i.GetName()<<' ';
                cout<<i.GetSentNum()<<' ';
                cout<<i.GetWordNum()<<' ';
                cout<<i.GetAttr().Ish1();
                cout<<i.GetAttr().Ish2();
                cout<<i.GetAttr().Ish3();
                cout<<i.GetAttr().IsTitle();
                cout<<i.GetAttr().IsItalic();
                cout<<i.GetAttr().IsBold()<<endl;
        }
        arch.clear();
        Doc.clear();
        return 0;
}*/
