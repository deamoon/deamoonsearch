// Как Яндекс понимает положенные в кукис инфу о другом отображении страницы
// Страница только отличается в переводе интерфейса или сортировке
#include <stdio.h> 

#include <libxml/parser.h> 
#include <libxml/HTMLparser.h> 

#include <curl/curl.h>

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

#include <openssl/md5.h>
#include <fcntl.h>
#include <unistd.h>

#include <set>
#include <queue>

#define BUFSIZE (1025*16)

std::queue<std::string> queue_link;
std::set<std::string> set_file, set_link;

//g++ main.c `xml2-config --cflags --libs` -lcurl -lcrypto

bool SubStr(std::string &s, std::string sub){ // 1, sub - начало s
    bool q = 1;
    for(int i=0;i<sub.length();++i){
        if (s[i]!=sub[i]) {
            q = 0;
            break;
        }
    }
    return q;
}

int find_a(xmlNode * a_node, std::ofstream &out, std::string &url) {
  xmlNode *cur_node = NULL;
  
  for (cur_node = a_node; cur_node; cur_node = cur_node->next) {    
    if (cur_node->type == XML_ELEMENT_NODE) {
      if ( !xmlStrcmp(cur_node->name, (const xmlChar *)"a") ) {
        xmlAttrPtr attr = cur_node->properties;
        for(; attr; attr=attr->next) {
            if ( !xmlStrcmp(attr->name, (const xmlChar *)"href") ) {
                std::string link = (char *)attr->children->content;

                ///////////////////////// Создание из url и link рабочего link javascript
                if ((SubStr(link, "mailto")) || (SubStr(link, "javascript"))) continue; // выкидываем майлы
                if (SubStr(link, "http://")) { // абсолютная ссылка
                    if (!SubStr(link, "http://acm.timus.ru")) continue; // не на Тимус
                } else {
                    if (link[0]=='/') link = "http://acm.timus.ru" + link; else { // относительная ссылка с главной
                        int q = 1;
                        std::string url2(url);
                        for (int i=url2.size()-1;i>=0;--i) {
                            if (url2[i]=='/') {
                                url2.erase(i+1,url2.size()-1);
                                q = 0;
                                break;
                            }
                        } 
                        if (q) link = url2 + '/' + link; else link = url2 + link;    
                    }
                }    
                //////////////////////
                
                //if ((link.find("problem")==std::string::npos) && (link.find("forum")==std::string::npos) && (link.find("help")==std::string::npos)) continue; // подобие релевантности индексирования
                if ((link.find("problem")==std::string::npos) && (link.find("help")==std::string::npos)) continue; // больше не парсим форум автоматом
                
                if (!set_link.count(link)) {
                    queue_link.push(link); // Добавляем в очередь
                    set_link.insert(link); // Добавляем в set
                    std::string link2(link); // Разбираем случай link и link/
                    if (link2[link2.size()-1]=='/') {
                        link2.erase(link2.size()-1, 1);
                        set_link.insert(link2);
                    } else {
                        set_link.insert(link2+'/');
                    }    
                }    
                
            }    
        } 
        //xmlSetProp(cur_node, (const xmlChar *)"onload", (const xmlChar*)"alert('Hello libXML!');"); 
        //return 1;
      }
    }

    if ( find_a(cur_node->children, out, url) ) return 1;
  }
  
  return 0;
}

int curl_start(CURL *curl_handle) {
  /* нам не нужено уведомление о прогрессе */
  curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

  curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, 120);
  curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, 1);
  curl_easy_setopt(curl_handle, CURLOPT_AUTOREFERER, 1);
  curl_easy_setopt(curl_handle, CURLOPT_COOKIEJAR, "coo.txt");
  curl_easy_setopt(curl_handle, CURLOPT_COOKIEFILE,"coo.txt");
  
  return 0;
}

int libxml_start(xmlNode *root_element, htmlDocPtr &docPtr, std::ofstream &out, std::string &url) {
  root_element = xmlDocGetRootElement(docPtr);
  find_a(root_element, out, url);
  //htmlSaveFileFormat("after_parse.html", docPtr, "utf-8", 1);
  xmlFreeDoc(docPtr);
  return 0;
}

int parsing(std::string &url, std::string &file, std::ofstream &link_info, CURL *curl_handle) {
  FILE *bodyfile;
  bodyfile = fopen(file.c_str(),"w");  
    curl_easy_setopt(curl_handle,   CURLOPT_WRITEDATA, bodyfile);
    curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
    curl_easy_perform(curl_handle);
  fclose(bodyfile);
  
  xmlNode *root_element = NULL;    
  htmlDocPtr docPtr = htmlParseFile(file.c_str(), "utf-8");
  libxml_start(root_element, docPtr, link_info, url);
  xmlCleanupParser();
}

std::string md5(std::string &file) {

	MD5_CTX c; /* контекст хэша */
	unsigned char buf[BUFSIZE];
	unsigned char md_buf[MD5_DIGEST_LENGTH];
        int i;
        
	/* В командной строке передается имя файла, для которого вычисляется хэш */
	int inf = open(file.c_str(), O_RDWR);

	/* Инициализируем контекст */
	MD5_Init(&c);

	/* Вычисляем хэш */
	for(;;) {
	    i = read(inf, buf, BUFSIZE);
	    if(i <= 0) break;
	    MD5_Update(&c, buf, (unsigned long)i);
	} 

	/* Помещаем вычисленный хэш в буфер md_buf */
	MD5_Final(md_buf, &c);

	/* Отображаем результат */
	char s[1]; std::string res;
	for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
	    sprintf(s, "%02x", md_buf[i]);
	    res += s;
	}   
	//std::cout<<res<<'\n'; 
	close(inf);
	return res;
} 

void add_link(std::string s){
    queue_link.push(s); 
    set_link.insert(s); 
    set_link.insert(s+'/');
}

int main(int ac, char* av[]) {
  CURL *curl_handle;
  std::string file, url, hash_file;
  std::ofstream link_info ("link.info"), queue_info("queue.info"), repeat_info("repeat.info");;
  
  /* инициализация сессии curl */
  curl_handle = curl_easy_init();
  curl_global_init(CURL_GLOBAL_ALL);
  curl_start(curl_handle);

////////////////////////////////////////////////////////////////////////////
  
  ////////////////////// Подобие релевантности
  std::ifstream link_start ("link_start.info");
  std::string str_link;
  
  while (getline(link_start, str_link)) {
      add_link(str_link); // Иницилизация очереди
  }
  link_start.close();
  //////////////////////
  
  int k = 1, t = 0, max=10; std::ostringstream oss;
  
  //std::cin>>max;
  
  while (!queue_link.empty()) {
      //++t; if (t>max) break;
      oss<<k++; file = "Base/" + oss.str() + ".html"; oss.str(""); // получаем название файла по числу      
      
      url = queue_link.front();
      parsing(url, file, link_info, curl_handle);
      queue_link.pop();
      
      hash_file = md5(file);
      if (set_file.count(hash_file)) { 
          remove(file.c_str()); // Если файл есть в базе, удаляем
          --k;
          repeat_info<<url<<"\n"; 
      } else {
          set_file.insert(hash_file); 
          link_info<<url<<"\n";
      }    
      //link_info<<url<<"\n";
      //set_link.insert(url); set_link.insert(url+'/');
  }
  
  while (!queue_link.empty()) {
      queue_info<<queue_link.front()<<"\n";
      queue_link.pop();
  }
  
  // в queue.info очередь ссылок
/////////////////////////////////////////////////////////////////////////////  
  link_info.close(); repeat_info.close(); queue_info.close();
  curl_easy_cleanup(curl_handle);
  return 0;
} 



 
