#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <string>
using namespace std;

string IntToStr(int a){
    string s;
    while (a!=0) {
        s+=(char)((a%10) + int('0')); 
        a = a/10;
    }
    for (int i=0;i<s.length()/2;++i) swap(s[i],s[s.length()-i-1]);
    return s;
}

int main(){                       
    ofstream file("link_start.info");
    string s("http://acm.timus.ru/forum/thread.aspx?id=");
    
    for(int i=5050;i<=29473;++i){ // 2 константы страниц форума
        file<<s+IntToStr(i)<<'\n';
    }
    
    file.close();
    return 0;                            
}                            
