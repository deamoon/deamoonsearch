#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <iostream>
#include <string>
#include <signal.h>
#include <boost/python.hpp>

using namespace std;

string index_client(string mes) {
    int sock, rd, i;
    char buf[1024];
    struct sockaddr_in addr;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0) {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(3426); // или любой другой порт...
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("connect");
        exit(2);
    }

    string s("");
    send(sock, mes.c_str(), mes.size(), 0);
    rd = recv(sock, buf, 1024, 0);
    for (i=0;i<rd;++i) {
        s += buf[i];
    }
    close(sock);
    return s;
}


using namespace boost::python;

BOOST_PYTHON_MODULE(index_client) {
    boost::python::def("index_client", index_client);
}







