// Как Яндекс понимает положенные в кукис инфу о другом отображении страницы
// Страница только отличается в переводе интерфейса или сортировке
#include <stdio.h> 
#include <curl/curl.h>

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

//g++ fetcher_start.cpp -lcurl

int curl_start(CURL *curl_handle) {
  /* нам не нужено уведомление о прогрессе */
  curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

  curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, 120);
  curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, 1);
  curl_easy_setopt(curl_handle, CURLOPT_AUTOREFERER, 1);
  curl_easy_setopt(curl_handle, CURLOPT_COOKIEJAR, "coo.txt");
  curl_easy_setopt(curl_handle, CURLOPT_COOKIEFILE,"coo.txt");
  
  return 0;
}

int parsing(std::string &url, std::string &file, std::ofstream &link_info, CURL *curl_handle) {
  FILE *bodyfile;
  bodyfile = fopen(file.c_str(),"w");  
    curl_easy_setopt(curl_handle,   CURLOPT_WRITEDATA, bodyfile);
    curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
    curl_easy_perform(curl_handle);
  fclose(bodyfile);
}

int main(int ac, char* av[]) {
  CURL *curl_handle;
  std::string file, url, hash_file;
  std::ofstream link_info ("link.info");
  
  /* инициализация сессии curl */
  curl_handle = curl_easy_init();
  curl_global_init(CURL_GLOBAL_ALL);
  curl_start(curl_handle);

////////////////////////////////////////////////////////////////////////////
  
  ////////////////////// Подобие релевантности
  std::ifstream link_start ("link_start.info");
  std::string str_link;
  std::ostringstream oss;
  int k=0;
  
  while (getline(link_start, str_link)) {
      oss<<++k; file = "Base/" + oss.str() + ".html"; oss.str(""); // получаем название файла по числу      
      parsing(str_link, file, link_info, curl_handle);
      link_info<<str_link<<'\n';
  }
  link_start.close();
  //////////////////////
  
  link_info.close();
  curl_easy_cleanup(curl_handle);
  return 0;
}
