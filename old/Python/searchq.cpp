#include <string>
#include <boost/python.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>

std::string greet() { return "hello, world"; }
int square(int number) { return number * number; }

BOOST_PYTHON_MODULE(searchq) {
    boost::python::def("greet", greet);
    boost::python::def("square", square);
}
