#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

char *InputNames[100];// Массив для ключей
char *InputValues[100];// Массив для значений
int InputCount=0; // количество пар ключ=значение

// Функция переопределения неудобоваримых символов
// в строке str 
void ConvChars(char *str) {
    int x,y;
    char buff2[10000];
    char hexstr[5];
    for (x = 0, y = 0; x < strlen(str); x++, y++) {
        switch (str[x]) {
            case '+':
                buff2[y] = ' ';
            break;
            case '%':
                strncpy(hexstr, &str[x + 1], 2);
                x = x + 2;
                if( ((strcmp(hexstr,"26")==0)) || ((strcmp(hexstr,"3D")==0)) ) {
                    buff2[y]='%';
                    y++;
                    strcpy(buff2,hexstr);
                    y=y+2;
                    break;
                }
                buff2[y] = (char)strtol(hexstr, NULL, 16);
            break;
            default:
                buff2[y] = str[x];
            break;
        }
    }
    strcpy(str,buff2);
}

void ParseInput(char *Input)
{
    int i=0,k=0;
    ConvChars(Input); 
    while(Input[i++]!=0) {
        if(Input[i]=='=') {
            InputNames[InputCount]=new char[i-k+1];
            strncpy(InputNames[InputCount],&Input[k],i-k);
            k=i+1;
        }
        if((Input[i]=='&')||(Input[i]==0)) {
            InputValues[InputCount]=new char[i-k+1];
            strncpy(InputValues[InputCount],&Input[k],i-k);
            k=i+1;
            InputCount++;
        }
    }
    for(i=0;i<InputCount;i++)printf("%s: %s<br>",InputNames[i],InputValues[i]);
}

int main() {
    char *s=getenv("REQUEST_METHOD");
    char *endptr;
    int i;
    unsigned int contentlength;
    char buff[10000];
    char a,b;
    const char *len1 = getenv("CONTENT_LENGTH");
 
    if(s!=NULL) {
        if(strcmp(s,"GET")==0) {
            strcpy(buff,getenv("QUERY_STRING"));
        } else
        if(strcmp(s,"POST")==0) {
            contentlength=strtol(len1, &endptr, 10);
            fread(buff, contentlength, 1, stdin); 
        }
        printf("Content-type: text/html\n\n");
        ParseInput(buff);
    }
    return 0;
}
