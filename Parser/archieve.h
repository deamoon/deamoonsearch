#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
using namespace std;
class TArchieve {
private:
        int file_amount;
        vector<string> urls;
public:
        void LoadArchieve();
        string GetURL(int ID);
        void clear();
};
string IntToStr(int a);
struct attr {
private:
        bool h1;
        bool h2;
        bool h3;
        bool title;
        bool i;
        bool b;
public:
        void fillFlags(int mask);
        bool Ish1();
        bool Ish2();
        bool Ish3();
        bool IsTitle();
        bool IsItalic();
        bool IsBold();

};
class TDocument {
private:
        vector<attr> attrs;
        vector<int> sent_num;
        vector<int> word_num;
        vector<string> words;
        int ID;
        int sent_amount;
        int word_amount;
public:
        void LoadDocument(int ID_);
        class Iterator {
                private:
                        int pos;
                        TDocument* Doc;
                public:
                        void operator ++ ();
                        bool operator == (Iterator a);
                        Iterator(TDocument* Doc_, int pos_);
                        string GetName();
                        attr GetAttr();
                        int GetSentNum();
                        int GetWordNum();
                        int GetCurPos();
        };
        Iterator begin();
        Iterator end();
        Iterator it(int pos);
        void clear();
};
