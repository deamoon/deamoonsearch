#include <unordered_map>
#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <vector>
#include <utility>
#include <algorithm>
#include "dirent.h"

typedef int docidtype;
class TPos
{
public:
                docidtype docid;
        long long pos;
        TPos (void);
        TPos(docidtype& _docid, long long _pos);
};

class TIndex
{
        friend class TIndexIterator;
public:
      TIndex(std::string _keys_fn, std::string _heap_fn);
private:
      std::ifstream *keys, *heap;
      std::unordered_map <std::string, std::vector <TPos> > index;
};

class TIndexIterator
{
public:
        TIndexIterator(TIndex& Index, std::string Word);
        bool valid(void);
        void next(void);
        TPos getPos(void);
 
private:
        std::vector <TPos>::iterator cur, end;
};

void Build(std::string pathi, std::string patho, bool case_sensitive, std::string keys_fn, std::string heap_fn);

TIndex q("./o/keys.o0", "./o/heap.o0");
