#include <iostream>
#include <algorithm>
#include <string> 
#include <fstream> 
using namespace std;

int Qt(int a) {
	return a + 4294967000;
}

char lowcase(string &s, int i) {
	char c = s[i];
	int n = (unsigned int)c;
	if ( ((Qt(184) <= n) && (n <= Qt(215))) || (n == Qt(169)) ) {	
        if ( (Qt(184) <= n) && (n <= Qt(199)) ) s[i] = (char)(n+32);	
        if ( n==Qt(169) ) { s[i] = (char)(Qt(185)); s[i-1] = (char)(Qt(249)); }
        if ( (Qt(200) <= n) && (n <= Qt(215)) ) { s[i] = (char)(n-32); s[i-1] = (char)(Qt(249)); }		
	}
}

int main(int argc, char const *argv[])
{
    ifstream file("in.txt");
	
	getline(file, s);
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	for (int i=0;i<s.length();++i) {
        if ( ((unsigned int)s[i]==4294967248) && (i!=s.length()-1) ) {
            lowcase(s, i+1);	
            continue;
        }
	}
	cout<<s<<'\n';
	return 0;
}